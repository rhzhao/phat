/*  Copyright 2013 IST Austria
    Contributed by: Ulrich Bauer, Michael Kerber, Jan Reininghaus

    This file is part of PHAT.

    PHAT is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PHAT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PHAT.  If not, see <http://www.gnu.org/licenses/>. */

// This file contains a simple example that demonstrates the usage of the library interface

// wrapper algorithm that computes the persistence pairs of a given boundary matrix using a specified algorithm
#include <include/phat/compute_persistence_pairs.h>

// main data structure (choice affects performance)
#include <include/phat/representations/vector_heap.h>

// algorithm (choice affects performance)
#include <include/phat/algorithms/standard_reduction.h>
#include <include/phat/algorithms/chunk_reduction.h>
#include <include/phat/algorithms/row_reduction.h>
#include <include/phat/algorithms/twist_reduction.h>
#include <include/phat/algorithms/twist_with_v_reduction.h>
#include <include/phat/algorithms/vineyard_update.h>

#include <stdlib.h>
#include <time.h>

void print_vector(std::vector<phat::index>& vec)
{
    for (std::vector<phat::index>::iterator it = vec.begin(); it != vec.end(); it++)
        std::cout << *it << " ";
    std::cout << std::endl;
}

template< typename Representation >
void print_matrix(phat::boundary_matrix<Representation>& boundary_matrix, char* name, std::vector<phat::index> permutation)
{
    phat::column col;
    phat::index n_col = boundary_matrix.get_num_cols();
    std::printf("%s=zeros(%lld, %lld);", name, n_col, n_col);
    for( phat::index col_idx = 0; col_idx < n_col; col_idx++ ) {
        boundary_matrix.get_col(col_idx, col);
        for (phat::column::iterator it = col.begin(); it != col.end(); it++)
        {
            std::printf("%s(%lld, %lld) = 1;", name, *it + 1, col_idx + 1);
        }
    }
    std::cout << "idx = [";
    print_vector(permutation);
    std::cout << "] + 1;";
    std::printf("%s=%s(idx, :);%s=%s(:, idx);", name, name, name, name);
    std::cout << std::endl;
}

int main( int argc, char** argv ) 
{
    // first define a boundary matrix with the chosen internal representation
    phat::boundary_matrix< phat::vector_heap > boundary_matrix;
    phat::boundary_matrix< phat::vector_heap > reduction_matrix;

    boundary_matrix.load_binary("C:\\Users\\Roy\\OneDrive\\School\\Princeton 2016\\Topology Research\\PHAT\\examples\\torus.bin");
    // print some information of the boundary matrix:
    std::cout << std::endl;
    std::cout << "The boundary matrix has " << boundary_matrix.get_num_cols() << " columns: " << std::endl;
    /*
    for( phat::index col_idx = 0; col_idx < boundary_matrix.get_num_cols(); col_idx++ ) {
        std::cout << "Column " << col_idx << " represents a cell of dimension " << (int)boundary_matrix.get_dim( col_idx ) << ". ";
        if( !boundary_matrix.is_empty( col_idx ) ) {
            std::vector< phat::index > temp_col;
            boundary_matrix.get_col( col_idx, temp_col ); 
            std::cout << "Its boundary consists of the cells";
            for( phat::index idx = 0; idx < (phat::index)temp_col.size(); idx++ )
                std::cout << " " << temp_col[ idx ];
        }
        std::cout << std::endl;
    }
    */
    std::cout << "Overall, the boundary matrix has " << boundary_matrix.get_num_entries() << " entries." << std::endl;  
    
    // define the object to hold the resulting persistence pairs
    phat::persistence_pairs pairs;

    // choose an algorithm (choice affects performance) and compute the persistence pair
    // (modifies boundary_matrix)
    std::vector<phat::index> lowest_one_lookup;
    std::vector<phat::index> max_index_lookup(boundary_matrix.get_num_cols(), -1);
    std::vector<phat::index> permutation(boundary_matrix.get_num_cols(), -1);
    phat::twist_reduction_v_reduction reduce;
    reduce(reduction_matrix, boundary_matrix, lowest_one_lookup);

    std::cout << "Done with reducing." << std::endl;

    for (phat::index idx = 0; idx < boundary_matrix.get_num_cols(); idx++)
    {
        permutation[idx] = idx;
        if (lowest_one_lookup[idx] != -1)
            max_index_lookup[lowest_one_lookup[idx]] = idx;
    }

    phat::vineyard_update update;
    std::vector<int> swaps;
    int num_swaps = 10000000;
    srand((unsigned) time(NULL));
    for (int i = 0; i < num_swaps; i++)
        swaps.push_back(rand() % (boundary_matrix.get_num_cols() - 1));

    clock_t startTime = clock();

    for (int i = 0; i < swaps.size(); i++)
        update(reduction_matrix, boundary_matrix, lowest_one_lookup, max_index_lookup, swaps[i], permutation);
    for (int i = swaps.size() - 1; i >= 0; i--)
        update(reduction_matrix, boundary_matrix, lowest_one_lookup, max_index_lookup, swaps[i], permutation);

    std::cout << double( clock() - startTime ) / (double)CLOCKS_PER_SEC<< " seconds." << std::endl;

    pairs.clear();
    for( phat::index idx = 0; idx < boundary_matrix.get_num_cols(); idx++ ) {
        if( !boundary_matrix.is_empty( idx ) ) {
            phat::index birth = boundary_matrix.get_max_index( idx );
            phat::index death = idx;
            pairs.append_pair( birth, death );
        }
    }
    
    // sort the persistence pairs by birth index 
    //pairs.sort();

    // print the pairs:
    std::cout << std::endl;
    std::cout << "There are " << pairs.get_num_pairs() << " persistence pairs: " << std::endl;
    /*
    for( phat::index idx = 0; idx < pairs.get_num_pairs(); idx++ )
        std::cout << "Birth: " << pairs.get_pair( idx ).first << ", Death: " << pairs.get_pair( idx ).second << std::endl;
        */
}
