/*  Contributed By: Roy Zhao */

#pragma once

#include <include/phat/helpers/misc.h>
#include <include/phat/boundary_matrix.h>
#include <algorithm>

namespace phat {
    class vineyard_update {
    public:
        //Algorithm based on Vines and Vineyards by Updating Persistence in Linear Time by Cohen-Steiner, Edelsbrunner, Morozov (2006)
        //In DV = R, V is the reduction matrix, R is the boundary matrix
        //@permutation[idx] denotes the index in the original ordering of row/column idx.
        //@lowest_one_lookup[idx] denotes the current index of the column in reduction_matrix with current lowest one idx. Inverse of max_index_lookup(curr -> curr)
        //@max_index_lookup[idx] denote the index of the lowest one in column idx of reduction matrix. It is the inverse of lowest_one_lookup (curr -> curr)
        template< typename Representation >
        void operator () (boundary_matrix< Representation >& reduction_matrix, boundary_matrix< Representation >& boundary_matrix,
                          std::vector< index >& lowest_one_lookup, std::vector< index >& max_index_lookup, index swap_simplex,
                          std::vector<index>& permutation) {
            //Swaps index swap_simplex and swap_simplex+1
            //Perform the swapping
            index temp = permutation[swap_simplex];
            permutation[swap_simplex] = permutation[swap_simplex + 1];
            permutation[swap_simplex + 1] = temp;

            if (lowest_one_lookup[swap_simplex] != -1) //simplex swap_simplex is positive simplex
            {
                if (lowest_one_lookup[swap_simplex + 1] != -1) //simplex swap_simplex and swap_simplex + 1 are both positive simplices
                {
                    if (reduction_matrix.is_in_matrix(permutation[swap_simplex + 1], permutation[swap_simplex])) //We must add col swap_simplex+1 to swap_simplex
                    {
                        reduction_matrix.add_to(permutation[swap_simplex + 1], permutation[swap_simplex]);
                    }

                    if (boundary_matrix.is_in_matrix(permutation[swap_simplex + 1], permutation[lowest_one_lookup[swap_simplex + 1]])) //R is not reduced after permuting
                    {
                        if (lowest_one_lookup[swap_simplex] < lowest_one_lookup[swap_simplex + 1])
                        {
                            boundary_matrix.add_to(permutation[lowest_one_lookup[swap_simplex]], permutation[lowest_one_lookup[swap_simplex + 1]]);
                            reduction_matrix.add_to(permutation[lowest_one_lookup[swap_simplex]], permutation[lowest_one_lookup[swap_simplex + 1]]);

                            //Change max_index_lookup
                            max_index_lookup[lowest_one_lookup[swap_simplex]] = swap_simplex + 1;
                            max_index_lookup[lowest_one_lookup[swap_simplex + 1]] = swap_simplex;

                            //lowest_one_lookup swap_simplex and swap_simplex + 1 exchange values
                            index temp = lowest_one_lookup[swap_simplex];
                            lowest_one_lookup[swap_simplex] = lowest_one_lookup[swap_simplex + 1];
                            lowest_one_lookup[swap_simplex + 1] = temp;
                        }
                        else
                        {
                            boundary_matrix.add_to(permutation[lowest_one_lookup[swap_simplex + 1]], permutation[lowest_one_lookup[swap_simplex]]);
                            reduction_matrix.add_to(permutation[lowest_one_lookup[swap_simplex + 1]], permutation[lowest_one_lookup[swap_simplex]]);
                            //lowest_one_lookup and max_index_lookup indices do not exchange
                        }
                    }
                    else
                    {
                        //R is reduced after permuting

                        //lowest_one_lookup swap_simplex and swap_simplex + 1 exchange values
                        index temp = lowest_one_lookup[swap_simplex];
                        lowest_one_lookup[swap_simplex] = lowest_one_lookup[swap_simplex + 1];
                        lowest_one_lookup[swap_simplex + 1] = temp;

                        //Change max_index_lookup
                        max_index_lookup[lowest_one_lookup[swap_simplex]] = swap_simplex;
                        max_index_lookup[lowest_one_lookup[swap_simplex + 1]] = swap_simplex + 1;

                    }
                }
                else //simplex swap_simplex is positive, swap_simplex + 1 is negative or essential
                {
                    if (reduction_matrix.is_in_matrix(permutation[swap_simplex + 1], permutation[swap_simplex])) //We must add col swap_simplex+1 to swap_simplex to fix PVP
                    {
                        reduction_matrix.add_to(permutation[swap_simplex + 1], permutation[swap_simplex]);
                    }

                    //Update our lookup vectors
                    max_index_lookup[lowest_one_lookup[swap_simplex]] = swap_simplex + 1;
                    lowest_one_lookup[swap_simplex + 1] = lowest_one_lookup[swap_simplex];
                    lowest_one_lookup[swap_simplex] = -1;

                    //lowest one in col swap_simplex + 1 is transfered to col swap_simplex
                    index low_index = max_index_lookup[swap_simplex + 1];
                    if (low_index != -1)
                    {
                        lowest_one_lookup[low_index] = swap_simplex;
                        max_index_lookup[swap_simplex] = low_index;
                        max_index_lookup[swap_simplex + 1] = -1;
                    }
                }
            }
            else //simplex swap_simplex is a negative or essential simplex
            {
                if (lowest_one_lookup[swap_simplex + 1] != -1) //simplex swap_simplex is negative/essential, and swap_simplex + 1 is positive
                {
                    if (reduction_matrix.is_in_matrix(permutation[swap_simplex + 1], permutation[swap_simplex]))
                    {
                        //Add col swap_simplex + 1 to swap_simplex to make V upper-triangular
                        boundary_matrix.add_to(permutation[swap_simplex + 1], permutation[swap_simplex]);
                        reduction_matrix.add_to(permutation[swap_simplex + 1], permutation[swap_simplex]);

                        //Add col swap_simplex to swap_simplex + 1 to reduce R
                        boundary_matrix.add_to(permutation[swap_simplex], permutation[swap_simplex + 1]);
                        reduction_matrix.add_to(permutation[swap_simplex], permutation[swap_simplex + 1]);

                        //Since V[i, i + 1] initially, we know that R[i, lowest_one_lookup[swap_simplex + 1]] = 1
                        //Columns effectively unchanged and only difference is row swap_simplex and swap_simplex+1 permuted
                    }
                    else // V is upper-triangular
                    {
                        //R will be reduced
                        //Update our lookup vectors
                        if (max_index_lookup[swap_simplex] != -1)
                        {
                            lowest_one_lookup[max_index_lookup[swap_simplex]] = swap_simplex + 1;
                            max_index_lookup[swap_simplex + 1] = max_index_lookup[swap_simplex];
                            max_index_lookup[swap_simplex] = -1;
                        }
                    }
                    //update column lowest_one_lookup[swap_simplex + 1]
                    if (!boundary_matrix.is_in_matrix(permutation[swap_simplex + 1], permutation[lowest_one_lookup[swap_simplex + 1]])) //Changed from i + 1
                    {
                        lowest_one_lookup[swap_simplex] = lowest_one_lookup[swap_simplex + 1];
                        lowest_one_lookup[swap_simplex + 1] = -1;
                        max_index_lookup[lowest_one_lookup[swap_simplex]] = swap_simplex;
                    }
                }
                else //simplex swap_simplex and swap_simplex + 1 are both negative/essential
                {
                    if (reduction_matrix.is_in_matrix(permutation[swap_simplex + 1], permutation[swap_simplex]))
                    {
                        boundary_matrix.add_to(permutation[swap_simplex + 1], permutation[swap_simplex]);
                        reduction_matrix.add_to(permutation[swap_simplex + 1], permutation[swap_simplex]);
                        index low_index_1 = max_index_lookup[swap_simplex];
                        index low_index_2 = max_index_lookup[swap_simplex + 1];
                        if (low_index_1 < low_index_2)
                        {
                            //Update lookup vectors
                            if (low_index_1 != -1)
                                lowest_one_lookup[low_index_1] = swap_simplex + 1;
                            lowest_one_lookup[low_index_2] = swap_simplex;
                            max_index_lookup[swap_simplex] = low_index_2;
                            max_index_lookup[swap_simplex + 1] = low_index_1;
                        }
                        else
                        {
                            //Add col swap_simplex to swap_simplex + 1 to reduce R
                            boundary_matrix.add_to(permutation[swap_simplex], permutation[swap_simplex + 1]);
                            reduction_matrix.add_to(permutation[swap_simplex], permutation[swap_simplex + 1]);

                            //lowest_one_lookup and max_index_lookup do not change
                        }
                    }
                    else // No problem swapping
                    {
                        //Update lookup vectors
                        index low_index_1 = max_index_lookup[swap_simplex];
                        index low_index_2 = max_index_lookup[swap_simplex + 1];
                        //We should have lowest_one_lookup[low_index_1] = swap_simplex and lowest_one_lookup[low_index_2] = swap_simplex+1
                        if (low_index_1 != -1)
                            lowest_one_lookup[low_index_1] = swap_simplex + 1;
                        if (low_index_2 != -1)
                            lowest_one_lookup[low_index_2] = swap_simplex;
                        max_index_lookup[swap_simplex] = low_index_2;
                        max_index_lookup[swap_simplex + 1] = low_index_1;
                    }
                }
            }
        }
    };
}
